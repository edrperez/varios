-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.4.20-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.3.0.6337
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Volcando estructura para tabla elecciones.candidatos
DROP TABLE IF EXISTS `candidatos`;
CREATE TABLE IF NOT EXISTS `candidatos` (
  `codigo` int(2) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `participando` enum('Y','N') NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla elecciones.candidatos: ~10 rows (aproximadamente)
DELETE FROM `candidatos`;
/*!40000 ALTER TABLE `candidatos` DISABLE KEYS */;
INSERT INTO `candidatos` (`codigo`, `nombre`, `foto`, `participando`) VALUES
	(1, 'Arturo Morales', 'arturo.jpg', 'Y'),
	(2, 'Juan Singh', 'juan.jpg', 'Y'),
	(3, 'Luis Albert', 'luis.jpg', 'Y'),
	(4, 'Francisco Salmeron', 'francisco.jpg', 'Y'),
	(5, 'Amanda Portela', 'amanda.jpg', 'Y'),
	(6, 'Emilio Miguel', 'emilio.jpg', 'Y'),
	(7, 'Aida Alcalde', 'aida.jpg', 'Y'),
	(8, 'Vicente Sanchis', 'vicente.jpg', 'Y'),
	(9, 'Alejandro Izquierdo', 'alejandro.jpg', 'Y'),
	(10, 'Clorovaldo Parra', 'clorovaldo.jpg', 'Y');
/*!40000 ALTER TABLE `candidatos` ENABLE KEYS */;

-- Volcando estructura para tabla elecciones.opciones
DROP TABLE IF EXISTS `opciones`;
CREATE TABLE IF NOT EXISTS `opciones` (
  `nombre` varchar(50) NOT NULL,
  `valor` varchar(50) NOT NULL DEFAULT '',
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla elecciones.opciones: ~1 rows (aproximadamente)
DELETE FROM `opciones`;
/*!40000 ALTER TABLE `opciones` DISABLE KEYS */;
INSERT INTO `opciones` (`nombre`, `valor`) VALUES
	('fecha_limite', '03/09/2021 01:32PM'),
	('puesto_actual', '1');
/*!40000 ALTER TABLE `opciones` ENABLE KEYS */;

-- Volcando estructura para tabla elecciones.padron
DROP TABLE IF EXISTS `padron`;
CREATE TABLE IF NOT EXISTS `padron` (
  `codigo` int(5) unsigned NOT NULL,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla elecciones.padron: ~10 rows (aproximadamente)
DELETE FROM `padron`;
/*!40000 ALTER TABLE `padron` DISABLE KEYS */;
INSERT INTO `padron` (`codigo`, `nombre`) VALUES
	(3362, 'Buenaventura Alberdi Parra'),
	(3991, 'Pedro Quirós Porcel'),
	(4548, 'Irma Aguirre'),
	(4569, 'Ángel Galán España'),
	(4741, 'Edelmiro Abril Ortega'),
	(5833, 'Reina de Alfaro'),
	(6402, 'Blas Ricart Vallejo'),
	(7150, 'Aura Fajardo'),
	(7347, 'Ciriaco Timoteo Calleja Antúnez'),
	(8642, 'Inmaculada Navas Teruel');
/*!40000 ALTER TABLE `padron` ENABLE KEYS */;

-- Volcando estructura para tabla elecciones.puestos
DROP TABLE IF EXISTS `puestos`;
CREATE TABLE IF NOT EXISTS `puestos` (
  `codigo` int(2) unsigned NOT NULL,
  `nombre` varchar(30) NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla elecciones.puestos: ~7 rows (aproximadamente)
DELETE FROM `puestos`;
/*!40000 ALTER TABLE `puestos` DISABLE KEYS */;
INSERT INTO `puestos` (`codigo`, `nombre`) VALUES
	(0, ''),
	(1, 'Coordinador'),
	(2, 'Sub Coordinador'),
	(3, 'Secretario'),
	(4, 'Pro Secretario'),
	(5, 'Tesorero'),
	(6, 'Pro Tesorero'),
	(7, 'Vocal');
/*!40000 ALTER TABLE `puestos` ENABLE KEYS */;

-- Volcando estructura para tabla elecciones.verificacion
DROP TABLE IF EXISTS `verificacion`;
CREATE TABLE IF NOT EXISTS `verificacion` (
  `codigo` varchar(64) NOT NULL,
  `puesto` int(2) unsigned NOT NULL,
  `voto` text NOT NULL,
  KEY `FK_verificacion_puestos` (`puesto`),
  CONSTRAINT `FK_verificacion_puestos` FOREIGN KEY (`puesto`) REFERENCES `puestos` (`codigo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla elecciones.verificacion: ~1 rows (aproximadamente)
DELETE FROM `verificacion`;
/*!40000 ALTER TABLE `verificacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `verificacion` ENABLE KEYS */;

-- Volcando estructura para tabla elecciones.votos
DROP TABLE IF EXISTS `votos`;
CREATE TABLE IF NOT EXISTS `votos` (
  `codigo` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `candidato` int(2) unsigned NOT NULL,
  `puesto` int(2) unsigned NOT NULL,
  `fecha` datetime NOT NULL,
  PRIMARY KEY (`codigo`),
  KEY `FK_verificacion_candidatos` (`puesto`),
  KEY `FK_verificacion_padron` (`candidato`) USING BTREE,
  CONSTRAINT `FK_votos_candidatos` FOREIGN KEY (`candidato`) REFERENCES `candidatos` (`codigo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_votos_puestos` FOREIGN KEY (`puesto`) REFERENCES `puestos` (`codigo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla elecciones.votos: ~1 rows (aproximadamente)
DELETE FROM `votos`;
/*!40000 ALTER TABLE `votos` DISABLE KEYS */;
/*!40000 ALTER TABLE `votos` ENABLE KEYS */;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
