<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
date_default_timezone_set('America/Guatemala');
require __DIR__ . '/vendor/autoload.php';

$bdd   = ADOnewConnection('mysqli');
$bdd->connect('localhost', 'elecciones', 'LIGAT3Nu65du', 'elecciones');
#$bdd->debug = true;

function obtenerNavegadorIP(){
    $navegador = $_SERVER['HTTP_USER_AGENT']??null;
    $ip = isset($_SERVER['HTTP_CLIENT_IP']) ? $_SERVER['HTTP_CLIENT_IP'] : isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
    return ["ip" => $ip, "navegador" => $navegador];
}

function cifrar($texto, $clave)
{
    $cifrado = false;
    $clave = hash("sha256", $clave);
    $iv = substr(hash("sha256", bin2hex(file_get_contents("secreto.php"))), 0, 16);
    $cifrado = base64_encode(openssl_encrypt($texto, "AES-256-CBC", $clave, 0, $iv));
    return $cifrado;
}

function descifrar($texto, $clave)
{
    $descifrado = false;
    $clave = hash("sha256", $clave);
    $iv = substr(hash("sha256", bin2hex(file_get_contents("secreto.php"))), 0, 16);
    $descifrado = base64_decode($texto);
    $descifrado = openssl_decrypt($descifrado, "AES-256-CBC", $clave, 0, $iv);
    return $descifrado;
}

function esVotante($codigo, $bdd) {
    $sql = "select * from padron where codigo = ".$codigo;
    return $bdd->GetRow($sql);
}

function haVotado($votante, $puesto, $bdd) {
    $codigo = hash("sha256", $votante.bin2hex(file_get_contents("secreto.php")));
    $sql = "SELECT codigo, puesto FROM verificacion WHERE codigo = '".$codigo."' and puesto = ".$puesto;
    $resultado = $bdd->GetRow($sql);
    return $resultado;
}

function esPuestoActual($puesto, $bdd) {
    $sql = "select codigo from puestos where codigo = " . $puesto;
    $resultado = $bdd->execute($sql);
    $existe = $resultado->recordCount();
    if ($existe == 1) {
        return true;
    } else {
        return false;
    }
}

function votar($votante, $candidato, $puesto, $voto, $bdd) {
    $bdd->startTrans();
    $votos = [];
    $votos["candidato"] = $candidato;
    $votos["puesto"]  = $puesto;
	$votos["fecha"]  = date('Y-m-d H:i:s');
    $verificaciones = [];
    $verificaciones["codigo"] = hash("sha256", $votante.bin2hex(file_get_contents("secreto.php")));
    $verificaciones["puesto"] = $puesto;
    $verificaciones["voto"] = $voto;
    $bdd->autoExecute("votos",$votos,"INSERT");
    $bdd->autoExecute("verificacion",$verificaciones,"INSERT");
    
    return $bdd->completeTrans();
}

function responder($mensaje, $error = false) {
    print(json_encode(["error" => $error, "mensaje" => $mensaje]));
}

if (isset($_GET['deboActualizar'])) {
    $formatoFecha = 'd/m/Y h:iA';
    $fechaCliente = $_GET['fecha'];
    $fechaClienteVerificada = DateTime::createFromFormat($formatoFecha, $fechaCliente);

    if (($fechaClienteVerificada && $fechaClienteVerificada->format($formatoFecha) == $fechaCliente) == false) {
        responder("La fecha no está en un formato correcto.");
        exit();
    }
    $fechaActual = date('d/m/Y h:iA');

    if ($fechaActual > $fechaCliente) {
        responder("Ésta ronda de votación ha terminado. Actualizando...");
        exit();
    }
    exit();
}

if (isset($_GET['fechaLimite'])) {
    $sql = "SELECT valor FROM opciones WHERE nombre = 'fecha_limite'";
    $resultado = $bdd->GetOne($sql);
    responder($resultado);
    exit();
}

if (isset($_GET['puestoActual'])) {
    $sql = "SELECT puestos.codigo, puestos.nombre FROM opciones JOIN puestos ON opciones.valor = puestos.codigo WHERE opciones.nombre = 'puesto_actual'";
    $resultado = $bdd->GetRow($sql);
    print(json_encode(["codigo" => $resultado[0], "nombre" => $resultado[1]]));
    exit();
}

if (isset($_GET['candidatosDisponibles'])) {
    $sql = "select * from candidatos where participando = 'Y'";
    $resultado = $bdd->execute($sql);
    $candidatos = [];
    while ($r = $resultado->fetchRow())
    {
        $candidatos[] = ['codigo' => $r['codigo'], 'nombre' => $r['nombre'], 'foto' => $r['foto']];
    }
    shuffle($candidatos);
    print(json_encode($candidatos));
    exit();
}

if (isset($_GET['votar'])) {
    $candidato = filter_input(INPUT_POST, 'candidato', FILTER_VALIDATE_INT, array('options' => array('min_range' => 1, 'max_range' => 10)));
    $votante = filter_input(INPUT_POST, 'votante', FILTER_VALIDATE_INT, array('options' => array('min_range' => 1000, 'max_range' => 9999)));
    $puesto = filter_input(INPUT_POST, 'puesto', FILTER_VALIDATE_INT, array('options' => array('min_range' => 1, 'max_range' => 7)));

    $sqlf = "SELECT valor FROM opciones WHERE nombre = 'fecha_limite'";
    $resultadof = $bdd->GetOne($sqlf);
    $fechaActual = date('d/m/Y h:iA');
    
    if ($fechaActual >= $resultadof) {
        responder("Ésta ronda de votación ha terminado.", null);
        exit();
    }

    if ($candidato === false or $votante === false or $puesto === false) {
        responder("Código incorrecto.", true);
        exit();
    }
    
    if (esVotante($votante, $bdd) == false) {
        responder("Código inválido.", true);
        exit();
    }

    if (esPuestoActual($puesto, $bdd) == false) {
        responder("No se puede votar por ese puesto.", true);
        exit();
    }
    
    if (haVotado($votante, $puesto, $bdd) != false) {
        responder("Usted ya ha votado por este puesto.", true);
        exit();
    }
    
    $obnaip = obtenerNavegadorIP();
    $voto = array(
        "fecha" => time(),
        "puesto" => $puesto,
        "candidato" => $candidato,
        "ip" => $obnaip["ip"],
        "navegador" => $obnaip["navegador"]
    );
    
    $codigoVotante = hash("crc32c", $votante);
    
    $cifrado = cifrar(json_encode($voto), $codigoVotante);
    if ($cifrado == false) {
        responder("Error votando.", true);
        exit();
    }
    
    if (votar($votante, $candidato, $puesto, $cifrado, $bdd)) {
        responder("Su voto ha sido emitido. Este es su código de verificación, anótelo: <code>" . $codigoVotante . "</code>");
    } else {
        responder("Ha ocurrido un error emitiendo su voto.");
    }
    
    exit();
}

if (isset($_GET['verificar'])) {
    $votante = filter_input(INPUT_POST, 'votante', FILTER_VALIDATE_INT, array('options' => array('min_range' => 1000, 'max_range' => 9999)));
    $verificador = filter_input(INPUT_POST, 'verificador', FILTER_SANITIZE_STRING);

    if ($votante === false or $verificador === false) {
        responder("Código incorrecto.", true);
        exit();
    }
    if (esVotante($votante, $bdd) == false) {
        responder("Código inválido.", true);
        exit();
    }
    if (hash("crc32c", $votante) != $verificador) {
        responder("Verificador inválido.", true);
        exit();
    }
    
    $codigo = hash("sha256", $votante.bin2hex(file_get_contents("secreto.php")));
    
    $sql = "select voto from verificacion where codigo = '" . $codigo . "'";
    $resultado = $bdd->execute($sql);
    $existe = $resultado->recordCount();
    $votos = [];
    if ($existe > 0) {
        $sqlp = "select codigo, nombre from puestos where codigo > 0";
        $resultadop = $bdd->execute($sqlp);
        $puestos = [];
        while ($rp = $resultadop->fetchRow())
        {
            $puestos[$rp["codigo"]] = $rp["nombre"];
        }
        $sqlc = "select codigo, nombre from candidatos";
        $resultadoc = $bdd->execute($sqlc);
        $candidatos = [];
        while ($rc = $resultadoc->fetchRow())
        {
            $candidatos[$rc["codigo"]] = $rc["nombre"];
        }
        while ($r = $resultado->fetchRow())
        {
            $actual = json_decode(descifrar($r['voto'], $verificador), true);
            $votos[] = ["puesto" => $puestos[$actual["puesto"]],
                        "candidato" => $candidatos[$actual["candidato"]],
                        "fecha" => date('d/m/Y h:iA', $actual["fecha"]),
                        "ip" => $actual["ip"],
                        "navegador" => $actual["navegador"]
                        ];
            unset($actual);
        }
        responder($votos);
    } else {
        responder("No ha emitido votos.", true);
    }
    
    exit();
}