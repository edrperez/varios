<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
date_default_timezone_set('America/Guatemala');
require __DIR__ . '/vendor/autoload.php';

$bdd   = ADOnewConnection('mysqli');
$bdd->connect('localhost', 'elecciones', 'LIGAT3Nu65du', 'elecciones');
#$bdd->debug = true;

function responder($mensaje, $error = false) {
    print(json_encode(["error" => $error, "mensaje" => $mensaje]));
}

if (isset($_GET['puestos'])) {
    $sql = "SELECT puestos.codigo, puestos.nombre FROM opciones JOIN puestos ON opciones.valor = puestos.codigo WHERE opciones.nombre = 'puesto_actual'";
    $resultado = $bdd->GetRow($sql);
    $sql1 = "SELECT * FROM puestos WHERE codigo > 0 ORDER by codigo ASC";
    $resultado1 = $bdd->execute($sql1);
    $puestos = [];
    while ($r = $resultado1->fetchRow())
    {
        $puestos[$r[0]] = $r[1];
    }
    responder(["actual" => $resultado[0], "puestos" => $puestos]);
    exit();
}

if (isset($_GET['votacionActual'])) {
    $puesto = filter_input(INPUT_POST, 'puesto', FILTER_VALIDATE_INT, array('options' => array('min_range' => 1, 'max_range' => 7)));
    $formatoFecha = 'd/m/Y h:iA';
    $fecha = $_POST['fecha'];
    $fechaVerificada = DateTime::createFromFormat($formatoFecha, $fecha);

    if (($fechaVerificada && $fechaVerificada->format($formatoFecha) == $fecha) == false) {
        responder("La fecha no está en un formato correcto.");
        exit();
    }
    
    if ($puesto == false || $puesto == null) {
        responder("El puesto no es válido.");
        exit();        
    }
    
    $bdd->startTrans();
    $puesto_actual = [];
    $puesto_actual["valor"]  = $puesto;
    $condicionp = "nombre LIKE 'puesto_actual'";
    $fecha_limite = [];
    $fecha_limite["valor"]  = $fecha;
    $condicionf = "nombre LIKE 'fecha_limite'";
    
    $bdd->autoExecute('opciones',$puesto_actual,'UPDATE', $condicionp);
    $bdd->autoExecute('opciones',$fecha_limite,'UPDATE', $condicionf);
    
    if ($bdd->completeTrans()) {
        responder("Se ha guardado la configuración.");
    } else {
        responder("Error guardando la configuración.", true);
    }
    
    exit();
}

if (isset($_GET['eliminarVotos'])) {
    $puesto = filter_input(INPUT_POST, 'puesto', FILTER_VALIDATE_INT, array('options' => array('min_range' => 1, 'max_range' => 7)));
    if ($puesto == false || $puesto == null) {
        responder("El puesto no es válido.");
        exit();        
    }
    $bdd->startTrans();
    $sqlvo = "DELETE FROM votos WHERE puesto = " . $puesto;
    $resultadovo = $bdd->execute($sqlvo);
    $sqlve = "DELETE FROM verificacion WHERE puesto = " . $puesto;
    $resultadove = $bdd->execute($sqlve);
    
    if ($bdd->completeTrans()) {
        responder("Se han eliminado los votos.");
    } else {
        responder("Error eliminando votos.", true);
    }
}

if (isset($_GET['candidatosDisponibles'])) {
    $sql = "select codigo, nombre, participando from candidatos order by nombre";
    $resultado = $bdd->execute($sql);
    $candidatos = [];
    while ($r = $resultado->fetchRow())
    {
        $candidatos[] = ['codigo' => $r['codigo'], 'nombre' => $r['nombre'], 'participando' => $r['participando']];
    }
    responder($candidatos);
}

if (isset($_GET['removerCandidatos'])) {
    $candidato = filter_input(INPUT_POST, 'candidato', FILTER_VALIDATE_INT, array('options' => array('min_range' => 1, 'max_range' => 10)));
    $participando = filter_input(INPUT_POST, 'participando', FILTER_SANITIZE_STRING);

    if ($candidato == false || $candidato == null || $participando == false || $participando == null) {
        responder("Error configurando candidatos.");
        exit();        
    }
    
    if ($participando == 'Y') {
        $nuevo = 'Y';
    } else {
        $nuevo = 'N';
    }

    $bdd->startTrans();
    $candidato_modificar = [];
    $candidato_modificar["participando"]  = $nuevo;
    $condicion = "codigo = " . $candidato;
    
    $bdd->autoExecute('candidatos',$candidato_modificar,'UPDATE', $condicion);
    
    if ($bdd->completeTrans()) {
        responder("Se han modificado los candidatos.");
    } else {
        responder("Error modificando los candidatos.", true);
    }
    
    exit();
}

if (isset($_GET['resultados'])) {
    $sql = "SELECT * FROM puestos WHERE codigo > 0 ORDER by codigo ASC";
    $resultado = $bdd->execute($sql);
    $mensaje = [];

    $sqlp = "SELECT COUNT(*) AS total FROM padron";
    $resultadop = floatval($bdd->GetOne($sqlp));
    while ($r = $resultado->fetchRow())
    {
        $sql1 = "SELECT votos.candidato, candidatos.nombre, COUNT(*) AS cantidad FROM votos JOIN candidatos ON votos.candidato = candidatos.codigo WHERE votos.puesto = ".$r[0]." GROUP BY candidato ORDER BY cantidad DESC;";
        $resultado1 = $bdd->execute($sql1);
        if($resultado1->recordCount() > 0) {
            $subtotal = 0;
            while ($r1 = $resultado1->fetchRow())
            {
                $subtotal += floatval($r1[2]);
                $mensaje[$r["nombre"]]['datos'][] = ["candidato" => $r1[0], "nombre" => $r1[1], "cantidad" => $r1[2], "porcentaje" => round(($r1[2] * 100 / $resultadop), 2)];
            }
            $mensaje[$r["nombre"]]['total'] = $subtotal;
            $mensaje[$r["nombre"]]['abstencion'] = round((100 - $subtotal * 100 / $resultadop), 2);

        } else {
            $mensaje[$r["nombre"]] = [];
            $mensaje[$r["nombre"]]['total'] = 0;
            $mensaje[$r["nombre"]]['abstencion'] = 0;
        }
        $mensaje[$r["nombre"]]['padron'] = $resultadop;
    }

    responder($mensaje);
    exit();    
}