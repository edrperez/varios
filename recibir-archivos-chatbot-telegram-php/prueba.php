<?php

$recibido = file_get_contents('php://input');

if ($json = json_decode(file_get_contents('php://input'), true)) {
	$recibido = json_encode($json, JSON_PRETTY_PRINT);
} else {
	$recibido = json_encode($_POST, JSON_PRETTY_PRINT);
}

$mensaje = json_decode($recibido, true);

if ($mensaje['message']['chat']['id'] != 704875499) {
	exit();
}

$token = "2044303607:AAHjTfGEG-GBoe5GjFOTmii1ML29cK6XYY8";

$nombre_archivo = "";
$tipo = "archivo";

if (isset($mensaje['message']['document']['file_id'])) {
	$nombre_archivo = $mensaje['message']['document']['file_name'];
	$url_datos_archivo = "https://api.telegram.org/bot".$token."/getFile?file_id=".$mensaje['message']['document']['file_id'];
}

if (isset($mensaje['message']['audio']['file_id'])) {
	$nombre_archivo = $mensaje['message']['audio']['file_name'];
	$url_datos_archivo = "https://api.telegram.org/bot".$token."/getFile?file_id=".$mensaje['message']['audio']['file_id'];
	$tipo = "audio";
}

if (isset($mensaje['message']['photo'])) {
	$ultimo = end($mensaje['message']['photo']);
	$url_datos_archivo = "https://api.telegram.org/bot".$token."/getFile?file_id=".$ultimo['file_id'];
	$tipo = "foto";
}

$datos_archivo = json_decode(file_get_contents($url_datos_archivo), true);

if (!isset($datos_archivo['result']['file_path'])) {
	exit();
}
if ($tipo == "foto") {
	$nombre_archivo = basename($datos_archivo['result']['file_path']);
}

$url_archivo = "https://api.telegram.org/file/bot".$token."/".$datos_archivo['result']['file_path'];
file_put_contents($nombre_archivo,file_get_contents($url_archivo));