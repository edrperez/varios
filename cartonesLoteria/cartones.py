import sys
from random import sample
from PIL import Image, ImageDraw, ImageFont

cartones = []
total = 0
fecha = sys.argv[1]
totalCartones = int(sys.argv[2])

while True:
    if total >= totalCartones:
        break
    nuevo = sample(range(1, 41), 16)
    if total > 0:
        if sorted(cartones[-1]) == sorted(nuevo):
            continue
        else:
            cartones.append(nuevo)
    else:
        cartones.append(nuevo)
    total += 1

cn = 1
w, h = 173, 281
for c in cartones:
    carton = Image.new(mode = "RGB", size = (692,1164), color = (255, 255, 255))
    p = 0
    i = 0
    x = 0
    y = 40
    while i < 4:
        j = 0
        while j < 4:
            imagen = Image.open('separados/{0}.png'.format(c[p]))
            carton.paste(imagen,(x,y))
            j += 1
            p += 1
            x += w
        x = 0
        y += h
        i += 1

    rectangulo1 = [(0, 0), (692, 40)]
    dibujo = ImageDraw.Draw(carton)
    dibujo.rectangle(rectangulo1, fill ="#ffffff", outline="#808080")
    fuente1 = ImageFont.truetype("MonospaceTypewriter.ttf", 25)
    dibujo.text((10, 5), "Lotería {0} - Cartón Número: {1}".format(fecha,cn), font=fuente1, fill=(0, 0, 0))
    carton.save("cartones/carton{0}-{1}.png".format(cn, fecha.replace("/", "-")),"PNG")
    cn += 1
