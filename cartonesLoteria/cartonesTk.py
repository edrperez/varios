import sys, os
import tkinter as tk
from tkinter import colorchooser
from random import sample
from PIL import Image, ImageDraw, ImageFont

class Cartones(tk.Tk):
    def __init__(self):
        tk.Tk.__init__(self)
        #self.iconbitmap("icono.ico")
        self.rutaDirectorioSalida = os.getcwd()+"\cartones"
        self.rutaDirectorioEntrada = os.getcwd()+"\separados"
        self.colorFondo = (0, "#f1f1f1")
        self.colorLinea = (0, "#808080")
        self.fecha = ""
        self.totalCartones = 1
        self.columnas = 2
        self.filas = 2
        self.totalCasillasIndividuales = 0
        self.title("Generador de cartones de lotería")
        self.cajaOpciones = tk.Frame(self)
        self.cajaOpciones.pack(side=tk.TOP)
        self.labelFecha = tk.Label(self.cajaOpciones, text="Fecha", width=10)
        self.labelFecha.pack(side=tk.LEFT)
        self.cajaFecha = tk.Entry(self.cajaOpciones)
        self.cajaFecha.pack(side=tk.LEFT)
        self.labelTotalCartones = tk.Label(self.cajaOpciones, text="Total de cartones", width=13)
        self.labelTotalCartones.pack(side=tk.LEFT)
        self.cajaTotalCartones = tk.Spinbox(self.cajaOpciones, from_=1, to=10000)
        self.cajaTotalCartones.pack(side=tk.LEFT)
        self.labelColumnas = tk.Label(self.cajaOpciones, text="Columnas", width=10)
        self.labelColumnas.pack(side=tk.LEFT)
        self.cajaColumnas = tk.Spinbox(self.cajaOpciones, from_=2, to=20)
        self.cajaColumnas.pack(side=tk.LEFT)
        self.labelFilas = tk.Label(self.cajaOpciones, text="Filas", width=10)
        self.labelFilas.pack(side=tk.LEFT)
        self.cajaFilas = tk.Spinbox(self.cajaOpciones, from_=1, to=20)
        self.cajaFilas.pack(side=tk.LEFT)
        self.botones = tk.Frame(self)
        self.botones.pack(side=tk.TOP)
        self.btnDirectorioEntrada = tk.Button(self.botones, text="Directorio de entrada: "+self.rutaDirectorioEntrada, command=self.directorioEntrada)
        self.btnDirectorioEntrada.pack(side=tk.LEFT)
        self.btnDirectorioSalida = tk.Button(self.botones, text="Directorio de salida: "+self.rutaDirectorioSalida, command=self.directorioSalida)
        self.btnDirectorioSalida.pack(side=tk.LEFT)
        self.btnColorFondo = tk.Button(self.botones, text="Color de fondo", command=self.elegirColorFondo)
        self.btnColorFondo.configure(bg=self.colorFondo[1])
        self.btnColorFondo.pack(side=tk.LEFT)
        self.btnColorLinea = tk.Button(self.botones, text="Color de línea", command=self.elegirColorLinea)
        self.btnColorLinea.configure(bg=self.colorLinea[1])
        self.btnColorLinea.pack(side=tk.LEFT)
        self.btnGenerar = tk.Button(self.botones, text="Generar", command=self.generar)
        self.btnGenerar.pack(side=tk.LEFT)
        

    def generar(self):
        self.fecha = self.cajaFecha.get()
        self.totalCartones = int(self.cajaTotalCartones.get())
        self.columnas = int(self.cajaColumnas.get())
        self.filas = int(self.cajaFilas.get())
        muestra = self.columnas * self.filas
        self.totalCasillasIndividuales = 0
        for file in os.listdir(self.rutaDirectorioEntrada):
            if file.endswith('.png'):
                self.totalCasillasIndividuales = self.totalCasillasIndividuales + 1
        cartones = []
        total = 0
        self.totalCasillasIndividuales = self.totalCasillasIndividuales + 1

        if muestra > self.totalCasillasIndividuales:
            tk.messagebox.showerror(title="Error", message="Tenés pocas imagenes/casillas individuales para el cartón que queres hacer")
        else:
            while True:
                if total >= self.totalCartones:
                    break
                nuevo = sample(range(1, self.totalCasillasIndividuales), muestra)
                if total > 0:
                    if sorted(cartones[-1]) == sorted(nuevo):
                        continue
                    else:
                        cartones.append(nuevo)
                else:
                    cartones.append(nuevo)
                total += 1

            cn = 1
            #ancho y alto en pixeles de cada imagen que va a ocupar una casilla en el cartón
            w, h = 173, 281
            ancho = w * self.columnas
            #más 40 pixeles para el espacio superior
            alto = h * self.filas + 40
            for c in cartones:
                carton = Image.new(mode = "RGB", size = (ancho,alto), color = (255, 255, 255))
                p = 0
                i = 0
                x = 0
                y = 40
                while i < self.filas:
                    j = 0
                    while j < self.columnas:
                        imagen = Image.open(self.rutaDirectorioEntrada+'/{0}.png'.format(c[p]))
                        carton.paste(imagen,(x,y))
                        j += 1
                        p += 1
                        x += w
                    x = 0
                    y += h
                    i += 1

                rectangulo1 = [(0, 0), (692, 40)]
                dibujo = ImageDraw.Draw(carton)
                dibujo.rectangle(rectangulo1, fill=self.colorFondo[1], outline=self.colorLinea[1])
                #fuente1 = ImageFont.truetype("MonospaceTypewriter.ttf", self.tamanoFuente)
                fuente1 = ImageFont.truetype("MonospaceTypewriter.ttf", 25)
                dibujo.text((10, 5), "Lotería {0} - Cartón Número: {1}".format(self.cajaFecha.get(),cn), font=fuente1, fill=(0, 0, 0))
                carton.save(self.rutaDirectorioSalida+"/carton{0}-{1}.png".format(cn, self.cajaFecha.get().replace("/", "-")),"PNG")
                cn += 1

    def directorioEntrada(self):
        self.rutaDirectorioEntrada = tk.filedialog.askdirectory()
        self.btnDirectorioEntrada.configure(text="Directorio de entrada: " + self.rutaDirectorioEntrada)

    def directorioSalida(self):
        self.rutaDirectorioSalida = tk.filedialog.askdirectory()
        self.btnDirectorioSalida.configure(text="Directorio de salida: " + self.rutaDirectorioSalida)

    def elegirColorFondo(self):
        self.colorFondo = colorchooser.askcolor(title ="Elegir color de fondo")
        self.btnColorFondo.configure(bg=self.colorFondo[1])
        

    def elegirColorLinea(self):
        self.colorLinea = colorchooser.askcolor(title ="Elegir color de línea")
        self.btnColorLinea.configure(bg=self.colorLinea[1])

    def cambiarTamanoFuente(self):
        self.labelTamanoFuente.configure(font=("MonospaceTypewriter.ttf",self.cajaTamanoFuente.get()))

if __name__ == "__main__":
    app = Cartones()
    app.mainloop()