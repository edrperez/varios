<?php

namespace Wabot;
/*El par "uuid" (custom_id) debe ser único por mensaje para llevar la secuencia de la conversación, eso está explicado en la documentación.*/
class Responder extends Waboxapp {

    public function enviarChat($respuesta, $destinatario, $codigo = 200) {
        header("Expires: Wed, 1 Jan 1997 07:00:00 GMT");
        header("Cache-Control: no-cache");
        header("Pragma: no-cache");
        header('Content-Type: application/json', false, $codigo);
        $uuid = "98asdfk32r";

        $datos_enviados = array(
            'token' => $this->obtener_token(),
            'uid' => $this->obtener_uid(),
            'to' => $destinatario,
            'custom_id' => $uuid,
            'text' => $respuesta
        );

        $this->enviarHaciaElApi('chat', $datos_enviados);
    }

    public function enviarMedia($destinatario, $url, $caption = null, $description = null) {
        header("Expires: Wed, 1 Jan 1997 07:00:00 GMT");
        header("Cache-Control: no-cache");
        header("Pragma: no-cache");
        header('Content-Type: application/json');
        $uuid = "98asdfk32r";

        $waboxapp = new Waboxapp();
        $datos_enviados = array(
            'token' => $waboxapp->obtener_token(),
            'uid' => $waboxapp->obtener_uid(),
            'to' => $destinatario,
            'custom_id' => $uuid,
            'url' => $url,
            'caption' => $caption,
            'description' => $description
        );

        $this->enviarHaciaElApi('media', $datos_enviados);
    }

    public function enviarLink($destinatario, $url, $caption = null, $description = null) {
        header("Expires: Wed, 1 Jan 1997 07:00:00 GMT");
        header("Cache-Control: no-cache");
        header("Pragma: no-cache");
        header('Content-Type: application/json');
        $uuid = "98asdfk32r";

        $waboxapp = new Waboxapp();
        $datos_enviados = array(
            'token' => $waboxapp->obtener_token(),
            'uid' => $waboxapp->obtener_uid(),
            'to' => $destinatario,
            'custom_id' => $uuid,
            'url' => $url,
            'caption' => $caption,
            'description' => $description
        );

        $this->enviarHaciaElApi('link', $datos_enviados);
    }

    public function enviarImage($destinatario, $url, $caption = null, $description = null) {
        header("Expires: Wed, 1 Jan 1997 07:00:00 GMT");
        header("Cache-Control: no-cache");
        header("Pragma: no-cache");
        header('Content-Type: application/json');
        $uuid = "98asdfk32r";

        $waboxapp = new Waboxapp();
        $datos_enviados = array(
            'token' => $waboxapp->obtener_token(),
            'uid' => $waboxapp->obtener_uid(),
            'to' => $destinatario,
            'custom_id' => $uuid,
            'url' => $url,
            'caption' => $caption,
            'description' => $description
        );
        $this->enviarHaciaElApi('image', $datos_enviados);
    }

    public function emoti($emoti) {
        return mb_convert_encoding($emoti, 'UTF-8', 'HTML-ENTITIES');
    }

    private function enviarHaciaElApi($endpoint, $payload) {
        $payload = json_encode($payload);
        $ch = curl_init();

        # Sólo para desarrollo las siguientes 2 líneas
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, $this->obtener_api_url() . $endpoint);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 25);

        $respuesta_servidor = curl_exec($ch);
        $respuesta_servidor_arreglo = json_decode($respuesta_servidor, true);

        if ($respuesta_servidor_arreglo["success"] === true) {
            $fp = file_put_contents('envios_waboxapp.log', date('Y-m-d H:i:s', time()) . "|" . serialize($payload) . "|".serialize(curl_getinfo($ch))."|" . $respuesta_servidor . "\n", FILE_APPEND);
        } else {
            $fp = file_put_contents('errores_waboxapp.log', date('Y-m-d H:i:s', time()) . "|Error curl: " . curl_error($ch) . "|" . serialize($payload) . "|".serialize(curl_getinfo($ch))."|" . $respuesta_servidor . "\n", FILE_APPEND);
        }

        curl_close($ch);
    }

}
