<?php

namespace Wabot;

class Pedidos extends Responder {

    public function listar($destinatario) {

        /*Busco en la base de datos o lo que sea*/
		$pedidosArreglo = ["trípode"];

        $mensaje = "";
        if (!empty($pedidosArreglo)) {
            $pedidos = implode("\r\n", $pedidosArreglo);
            $mensaje .= $pedidos . "\r\n\r\nEstos son tus pedidos.";
        } else {
            $mensaje = "No encontré pedidos disponibles.";
        }
        $this->enviarChat($mensaje, $destinatario);
    }

}
