<?php

namespace Wabot;

require_once "waboxapp.php";
require_once "escuchar.php";
require_once "responder.php";
require_once "pedidos.php";
require_once "productos.php";
require_once "facturas.php";

$escuchar = new Escuchar();
$responder = new Responder();

$recibido = file_get_contents('php://input');

if ($json = json_decode(file_get_contents("php://input"), true)) {
    $recibido = $json;
} else {
    $recibido = $_POST;
}

# Validar que la estructura a recibir sea la correcta
$waboxapp = new Waboxapp();
$waboxapp->esChat($recibido);

if ($waboxapp->esChat($recibido) == false) {
    exit;
}

$mensaje = $recibido['message']['body']['text'];
$destinatario = $recibido['contact']['uid'];

if ($escuchar->mensaje('/.*hola.*/i', $mensaje)) {
    $responder->enviarChat("Hola qué tal, soy un chatbot.", $destinatario);
    exit;
}

if ($escuchar->mensaje('/.*gracias.*/i', $mensaje)) {
    $responder->enviarChat("De nada, ha sido un gusto servirte " . $responder->emoti('&#128522;') . ".", $destinatario);
    exit;
}

if ($escuchar->mensaje('/.*web.*/i', $mensaje)) {
    $responder->enviarLink($destinatario, "https://www.edrperez.com", "Nuestro sitio Web", "Encontrarás más información sobre nuestros productos.");
    exit;
}

# Pedidos
if ($escuchar->mensaje('/.*1.*/', $mensaje)) {
    $pedidos = new Pedidos();
    $pedidos->listar($destinatario);
    exit;
}

# Productos
if ($escuchar->mensaje('/.*2.*/', $mensaje)) {
    $productos = new Productos();
    $productos->listar($destinatario);
    exit;
}

# Facturas
if ($escuchar->mensaje('/.*3.*/', $mensaje)) {
    $facturas = new Facturas();
    $facturas->imprimir($destinatario);
    exit;
}

# Ninguno
$responder->enviarChat("No entendí " . $responder->emoti('&#128517;') . ". Mi menú es: \r\n*1*. Pedidos\r\n*2*. Productos\r\n*3*. Facturas", $destinatario);