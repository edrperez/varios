<?php
if (isset($_GET["chat"]) || isset($_GET["media"]) || isset($_GET["link"])) {
	$recibido = file_get_contents('php://input');

	if ($json = json_decode(file_get_contents("php://input"), true)) {
		$recibido = $json;
	} else {
		$recibido = $_POST;
	}

	$respuesta = ['success' => true, 'custom_id' => uniqid()];

	header("Expires: Wed, 1 Jan 1997 07:00:00 GMT");
	header("Cache-Control: no-cache");
	header("Pragma: no-cache");
	header('Content-Type: application/json');
	print(json_encode($respuesta));
}