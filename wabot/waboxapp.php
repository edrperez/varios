<?php

namespace Wabot;

class Waboxapp {

    protected $token = 's09d8098dfj900sd9809s8d09f809s8f09i4b98n56n4546b';
    protected $uid = '50211223344';
    protected $api_url = 'https://www.waboxapp.com/api/send/';
	#protected $api_url = 'http://localhost/wabot/mock?';

    public function obtener_token() {
        return $this->token;
    }

    public function obtener_uid() {
        return $this->uid;
    }

    public function obtener_api_url() {
        return $this->api_url;
    }

    private function compararArreglos(array $arr1, array $arr2) {
        #https://www.php.net/manual/en/function.array-diff-key.php#112558
        $diff = array_diff_key($arr1, $arr2);
        $intersect = array_intersect_key($arr1, $arr2);

        foreach ($intersect as $k => $v) {
            if (is_array($arr1[$k]) && is_array($arr2[$k])) {
                $d = $this->compararArreglos($arr1[$k], $arr2[$k]);

                if ($d) {
                    $diff[$k] = $d;
                }
            }
        }

        return $diff;
    }

    public function esChat($recibido) {
        $esperado = json_decode('{"event":"message","token":"asdf","uid":"12312345678","contact":{"uid":"645","name":"akjsdlfk","type":"user"},"message":{"dtm":1487082303,"uid":"546asd4f6","cuid":"","dir":"i","type":"chat","body":{"text":"ñandú"},"ack":3}}', true);

        if (empty($this->compararArreglos($recibido, $esperado))) {
            if ($recibido['token'] == $this->obtener_token() && $recibido['message']['dir'] == "i") {
                return true;
            }
            return false;
        }
        return false;
    }

}
