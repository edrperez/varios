import pyttsx3, sys

engine = pyttsx3.init()
engine.setProperty('rate', 160)
engine.setProperty('voice', 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Speech\Voices\Tokens\TTS_MS_ES-MX_SABINA_11.0')

with open(sys.argv[1], 'r',encoding='utf-8') as archivo:
    texto = archivo.read().replace('\n', ' ')
#texto = "Hola, ¿qué tal estás?"
#engine.say(texto)
engine.save_to_file(texto, 'prueba.mp3')
engine.runAndWait()
