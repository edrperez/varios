<?php

/*
 * Copyright (C) 2015 Edgar Pérez <contacto@edgarperezgonzalez.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
$imagen_decodificada = base64_decode(filter_input(INPUT_POST, 'imagen'));
$dpi = filter_input(INPUT_POST, 'dpi', FILTER_SANITIZE_SPECIAL_CHARS);
$nombre = filter_input(INPUT_POST, 'nombre', FILTER_SANITIZE_SPECIAL_CHARS);

try {
    $dbh = new PDO("mysql:host=localhost;dbname=pruebas", "usuario", "clave");

    $query = $dbh->prepare("INSERT INTO webcam_fotografias"
            . "(dpi, nombre, fotografia)"
            . "values (:dpi, :nombre, :fotografia)");

    $query->bindParam(':dpi', $dpi);
    $query->bindParam(':nombre', $nombre);
    $query->bindParam(':fotografia', $imagen_decodificada, PDO::PARAM_LOB);
    $query->execute();

    echo "Datos salvados.";

    $dbh = null;
} catch (PDOException $e) {
    echo $e->getMessage();
}
?>