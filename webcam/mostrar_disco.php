<?php

/*
 * Copyright (C) 2015 Edgar Pérez <contacto@edgarperezgonzalez.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
try {
    $dbh = new PDO("mysql:host=localhost;dbname=pruebas", "usuario", "clave");
    $sql = "SELECT * FROM webcam_disco";

    echo "<h1>Listado de personas</h1>";

    foreach ($dbh->query($sql) as $fila) {
        echo 'DPI: ' . $fila['dpi'] . '<br/>';
        echo 'Nombre:' . $fila['nombre'] . '<br />';
        echo 'Fotografía:<br/><img src="imagenes/' . $fila['archivo'] . '"><br /><br />';
    }

    $dbh = null;
} catch (PDOException $e) {
    echo $e->getMessage();
}
?>