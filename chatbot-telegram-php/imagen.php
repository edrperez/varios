<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
date_default_timezone_set('America/Guatemala');

$imagen = 'url o dirección de la imagen';
$token = "aquí va el token del chatbot";

$datos = [
    'chat_id' => 'Aquí va el id númerico del destinatario',
    #'chat_id' => '@el_canal si va dirigido a un canal',
    'photo' => $imagen,
    'caption' => 'La descripción de la imagen'
];
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, "https://api.telegram.org/bot" . $token . "/sendPhoto");
curl_setopt($ch, CURLOPT_HEADER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POST, TRUE);
curl_setopt($ch, CURLOPT_POSTFIELDS, $datos);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$r_array = json_decode(curl_exec($ch), true);

curl_close($ch);
if ($r_array['ok'] == 1) {
    echo "Imagen enviada.";
} else {
    echo "Imagen no enviada.";
    print_r($r_array);
}
