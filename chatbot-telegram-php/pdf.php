<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
date_default_timezone_set('America/Guatemala');

$archivo = 'el nombre del documento.pdf';
$token = "aquí va el token del chatbot";

$documento = new CURLFile(realpath($archivo));
$documento->setPostFilename($archivo);
$datos = [
    'chat_id' => 'Aquí va el id númerico del destinatario',
    #'chat_id' => '@el_canal si va dirigido a un canal',
    'document' => $documento,
    'caption' => 'Te adjunto un documento muy importante'
];
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, "https://api.telegram.org/bot" . $token . "/sendDocument");
curl_setopt($ch, CURLOPT_HEADER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POST, TRUE);
curl_setopt($ch, CURLOPT_POSTFIELDS, $datos);
curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type:multipart/form-data']);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$r_array = json_decode(curl_exec($ch), true);

curl_close($ch);
if ($r_array['ok'] == 1) {
    echo "Documento enviado.";
} else {
    echo "Documento no enviado.";
    print_r($r_array);
}