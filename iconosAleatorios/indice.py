import glob, json
directorio = 'icons/'

def generar_listado(direc, listadoAr = 'listado.json'):
    i = 1
    listado = {}
    for nombreArchivo in glob.iglob(direc + '**/*.png', recursive=True):
         listado[i] = nombreArchivo
         i += 1

    with open(listadoAr, 'w') as salida:
        json.dump(listado, salida, indent=4)

generar_listado(directorio)
