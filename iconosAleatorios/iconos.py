import json, os, random, sys
from PIL import ImageTk,Image
import pygame

pygame.init()
pantalla = pygame.display.set_mode((778, 900))
pygame.display.set_caption('Generador aleatorios game-icons.net')

def leer_listado(listadoAr = 'listado.json'):
    with open(listadoAr) as jsonArchivo:        
        datos = json.load(jsonArchivo)
    return datos

def generar_imagenes():
    listado = leer_listado()
    totalListado = len(listado.keys())

    clavesAleatorias = random.sample(range(1, totalListado), 9)
    imagenes = {}

    for i in range(9):
        imagen = pygame.image.load(listado[str(clavesAleatorias[i])]).convert_alpha()
        pa = pygame.PixelArray(imagen)
        pa.replace((255, 255, 255), (random.randint(10, 245), random.randint(10, 245), random.randint(10, 245)), distance=0.8)
        del pa
        imagen = pygame.transform.scale(imagen, (256, 256))
        nombre = os.path.splitext(os.path.basename(listado[str(clavesAleatorias[i])]))[0].replace("-", " ")
        imagenes[i] = {"imagen" : imagen, "nombre" : nombre}
    totalImagenes = len(imagenes.keys())
    return imagenes

imagenes = generar_imagenes()
fuenteNombres = pygame.font.SysFont("monospace", 20)
fuenteBotones = pygame.font.SysFont('Arial',35)
salir = fuenteBotones.render('Salir', True, (0, 0, 0))
regenerar = fuenteBotones.render('Regenerar', True, (0, 0, 0))

while True :
    pantalla.fill((255, 255, 255))
    k = 0
    x = 0
    y = 20
    for i in range(3):
        j = 0
        x = 0
        for j in range(3):
            etiqueta = fuenteNombres.render(imagenes[k]["nombre"], 1, (255,0,0))
            pantalla.blit(etiqueta, (x, y-20))
            pantalla.blit(imagenes[k]["imagen"], (x, y))
            x += (256 + 5)
            k += 1
            j += 1
        y += (256 + 20)
        i += 1
  
    for evento in pygame.event.get() :
        if evento.type == pygame.QUIT :
            pygame.quit()
            quit()
        if evento.type == pygame.MOUSEBUTTONDOWN:
            if 680 <= puntero[0] <= 680+60 and 850 <= puntero[1] <= 850+40:
                pygame.quit()
                sys.exit(0)

        if evento.type == pygame.MOUSEBUTTONDOWN:
            if 128 <= puntero[0] <= 128+140 and 850 <= puntero[1] <= 850+40:
                imagenes = generar_imagenes()

    puntero = pygame.mouse.get_pos()

    if 680 <= puntero[0] <= 680+60 and 850 <= puntero[1] <= 850+40:
        pygame.draw.rect(pantalla,(160, 160, 160),[680,850,60,40])
    else:
        pygame.draw.rect(pantalla,(90, 90, 90),[680,850,60,40])

    if 128 <= puntero[0] <= 128+140 and 850 <= puntero[1] <= 850+40:
        pygame.draw.rect(pantalla,(160, 160, 160),[128,850,140,40])
    else:
        pygame.draw.rect(pantalla,(90, 90, 90),[128,850,140,40])

    pantalla.blit(salir, (680,850))
    pantalla.blit(regenerar, (128,850))
    pygame.display.update() 
